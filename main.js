const btnShare = document.getElementById("btn-share");
const footer = document.getElementById("card-footer");

btnShare.onclick = function (e) {
  e.preventDefault();
  footer.classList.toggle("active");
};
